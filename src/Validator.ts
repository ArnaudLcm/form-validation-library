export interface IConfigInfos<T> {
	val: T,
	label: string
}


/**
 * @brief The purpose of this class is to validate a set of inputs by returning a list of errors
 */

export class InputValidator {

	private errors: {[key: string]: string[]}
	private config: Record<string, IConfigInfos<any>>

	constructor(config: Record<string, IConfigInfos<any>>) {
		this.errors = {}
		this.config = config
	}


	isProvided(field: string) {
		if (!this.config[field] || this.config[field].val == undefined) {
			this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `Field ${this.config[field].label ?? "unknown"} is required.`]
		}
		return this
	}

	isString(field: string) {
		if (!this.config[field] || typeof this.config[field].val !== "string") {
			this.errors[field] = [...(this.errors[field] ? this.errors[field] : []), `Field ${this.config[field].label ?? "unknown"} is not a string.`]
		}
		return this
	}


	getErrors() {
		return this.errors
	}

}

