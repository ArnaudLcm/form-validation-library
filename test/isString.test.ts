import { InputValidator } from "../src/Validator";

describe('Check the isString condition', () => {
    test('Provide a string', () => {
        const config = {
            "myKey": {
                "val": "test",
                "label": "myKey"
            }
        }
        const validator = new InputValidator(config)
        expect(Object.keys(validator.isProvided("myKey").isString("myKey").getErrors())).toHaveLength(0);
    });

    test('Not providing a string', () => {
        const config = {
            "myKey": {
                "label": "myKey",
                "val": 2
            }
        }
        const validator = new InputValidator(config)
        expect(Object.keys(validator.isProvided("myKey").isString("myKey").getErrors())).toHaveLength(1);
    });
    
});