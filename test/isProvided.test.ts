import { InputValidator } from "../src/Validator";

describe('Check the isProvided condition', () => {
    test('Provide the field', () => {
        const config = {
            "myKey": {
                "val": "test",
                "label": "myKey"
            }
        }
        const validator = new InputValidator(config)
        expect(Object.keys(validator.isProvided("myKey").getErrors())).toHaveLength(0);
    });

    test('Not providing the field', () => {
        const config = {
            "myKey": {
                "label": "myKey",
                "val": undefined
            }
        }
        const validator = new InputValidator(config)
        expect(Object.keys(validator.isProvided("myKey").getErrors())).toHaveLength(1);
    });
    
});